# _Nannochloropsis_ Directed Evolution

## Background

### Whole Genome Sequencing data

- [A Step-By-Step Guide to DNA Sequencing Data Analysis](https://www.kolabtree.com/blog/a-step-by-step-guide-to-dna-sequencing-data-analysis/) (WGS short reads)

- Another QC tool used by one of my labmates: [fastp](https://github.com/OpenGene/fastp)
    - assess quality scores
    - perform dedup of identical reads
    - filter to Q30
    - merge overlapping PE reads
- Also consider [trim-galore](https://github.com/FelixKrueger/TrimGalore)

### Genomes

- [_Nannochloropsis oceanica_ (JGI MycoCosm)](https://mycocosm.jgi.doe.gov/Nanoce1779/Nanoce1779.home.html)
- [_Nannochloropsis oceanica_ (NCBI)](https://www.ncbi.nlm.nih.gov/datasets/genome/GCA_004519485.1/)
- [_Nannochloropsis sp._ QH25 (NCBI)](https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA793856)

### Whole genome sequencing

- [_Nannochloropsis sp._ QH25 (NCBI)](https://www.ncbi.nlm.nih.gov/bioproject/?term=PRJNA793856)
- [Osprey](ftp://ftp.lanl.gov/public/genome/algae_Osprey__QT001__strains_-_shotgun)

### Genomics papers

- [Genome assembly of _Nannochloropsis oceanica_ provides evidence of host nucleus overthrow by the symbiont nucleus during speciation](https://www.nature.com/articles/s42003-019-0500-9) (2019)
- [De novo whole genome sequencing data of two mangrove-isolated microalgae from Terengganu coastal waters](https://www.sciencedirect.com/science/article/pii/S2352340919310352) (2019)
- [Sequencing and comparative analysis of three Chlorella genomes provide insights into strain-specific adaptation to wastewater](https://www.nature.com/articles/s41598-019-45511-6) (2019)
- [Genome sequencing, assembly, and annotation of the self-flocculating microalga _Scenedesmus obliquus_ AS-6-11](https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-020-07142-4) (2020)
- [Large-scale genome sequencing reveals the driving forces of viruses in microalgal evolution (2021)](https://www.cell.com/cell-host-microbe/fulltext/S1931-3128(20)30667-3)
- [The Genome Sequence of an Algal Strain of _Nannochloropsis_ QH25](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC9753661/) (2022)
- [Genetic mechanisms underlying increased microalgal thermotolerance, maximal growth rate, and yield on light following adaptive laboratory evolution](https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-022-01431-y) (2022)
- [The genomes of _Vischeria_ oleaginous microalgae shed light on the molecular basis of hyper-accumulation of lipids](https://bmcbiol.biomedcentral.com/articles/10.1186/s12915-023-01618-x) (2023)

## Genomic analysis

### Read mapping

Aligning WGS short-read sequencing data to the QH25 reference genome with [minimap2](https://lh3.github.io/minimap2/minimap2.html).

Create and activate a conda environment
```sh
conda create -n directev -c conda-forge -c bioconda minimap2 \
  wget samtools fastp freebayes bcftools
conda activate directev
```

Download the QH25 reference genome with `curl`
```sh
curl -o ncbi_dataset.zip https://api.ncbi.nlm.nih.gov/datasets/v2alpha/genome/accession/GCA_024741955.1/download?include_annotation_type=GENOME_FASTA,GENOME_GFF,RNA_FASTA,CDS_FASTA,PROT_FASTA,SEQUENCE_REPORT
unzip ncbi_dataset.zip
mv ncbi_dataset/data/GCA_024741955.1/GCA_024741955.1_ASM2474195v1_genomic.fna ./
```

Download sequencing data with `wget`
```sh
wget ftp://ftp.lanl.gov/public/genome/algae_Osprey__QT001__strains_-_shotgun/3009_725/3009_725-A11_7277-5229__ACGTGAAC-TTTGGGAT__S8_R1_001.fastq.gz
wget ftp://ftp.lanl.gov/public/genome/algae_Osprey__QT001__strains_-_shotgun/3009_725/3009_725-A11_7277-5229__ACGTGAAC-TTTGGGAT__S8_R2_001.fastq.gz
```

Apply QC to the reads with `fastp`
```sh
fastp -i 3009_725-A11_7277-5229__ACGTGAAC-TTTGGGAT__S8_R1_001.fastq.gz \
  -I 3009_725-A11_7277-5229__ACGTGAAC-TTTGGGAT__S8_R2_001.fastq.gz \
  -o nanno_725_R1.fq.gz \
  -O nanno_725_R2.fq.gz
```

Align reads to the reference genome:
```sh
minimap2 -a -x sr -t 2 GCA_024741955.1_ASM2474195v1_genomic.fna \
  nanno_725_R1.fq.gz \
  nanno_725_R2.fq.gz \
  | samtools view -b -o nanno_725.bam -
```

### Variant calling

To prepare for variant calling, aligned BAM files need to be dedupped and sorted. To remove duplicate reads and sort:

```sh
samtools collate -o nanno_725_namecollate.bam nanno_725.bam
samtools fixmate -m nanno_725_namecollate.bam nanno_725_fixmate.bam
samtools sort -o nanno_725_sorted.bam nanno_725_fixmate.bam
samtools markdup -r nanno_725_sorted.bam nanno_725_markdup.bam
```

The file `nanno_725_markdup.bam` is ready to be passed on to variant calling. The current recommended tool is `freebayes`. The main documentation is on [GitHub](https://github.com/freebayes/freebayes), another tutorial is [here](https://hbctraining.github.io/In-depth-NGS-Data-Analysis-Course/sessionVI/lessons/02_variant-calling.html). To call variants against the reference genome:

```sh
freebayes -f GCA_024741955.1_ASM2474195v1_genomic.fna -p 1 nanno_725_markdup.bam > nanno_725.vcf
```

This assumes a ploidy of 1, you can change the value of the `-p` flag if it should be different. Applying freebayes to each of your samples will result in a VCF for each sample. You can merge them with `bcftools`, which has [documentation here](https://samtools.github.io/bcftools/bcftools.html) and a [walkthrough on merging here](https://www.biocomputix.com/post/how-to-combine-merge-vcf-bcf-files-using-bcftools-merge).

You can install bcftools in your conda environment like:

```sh
conda install -c conda-forge -c bioconda bcftools
```

The merged VCF file with all of your samples will be a good starting point for analyzing SNPs & INDELs across your experiment

## Pangenomic analysis

- [PanKmer](https://salk-tm.gitlab.io/pankmer/)
